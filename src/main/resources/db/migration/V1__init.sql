create table team
(
    team_id   int          not null AUTO_INCREMENT,
    team_name VARCHAR(244) NOT NULL,
    PRIMARY KEY (team_id)
);

CREATE TABLE match
(
    match_id      int not null AUTO_INCREMENT,
    total_balls   int not null,
    first_team    int not null,
    second_team   int not null,
    first_playing int not null,
    team_won      int,
    man_of_match  varchar(1024),
    FOREIGN KEY (first_team) REFERENCES team (team_id),
    FOREIGN KEY (second_team) REFERENCES team (team_id),
    FOREIGN KEY (first_playing) REFERENCES team (team_id)
);

create TABLE player
(
    id            int           not null AUTO_INCREMENT,
    name          varchar(1024) not null,
    team_id       int           not null,
    runs_scored   INTEGER,
    balls_played  INTEGER,
    wickets_taken INTEGER,
    balls_bowled  INTEGER,
    FOREIGN KEY (team_id) REFERENCES team (team_id),
);

CREATE table team_score
(
    id           int not null AUTO_INCREMENT,
    match_id     int not null,
    team_id      int not null,
    runs_scored  INTEGER,
    balls_played INTEGER,
    wickets_out  INTEGER,
    FOREIGN KEY (match_id) REFERENCES match (match_id),
    FOREIGN KEY (team_id) REFERENCES team (team_id)
);

create table player_score
(
    id            int not null AUTO_INCREMENT,
    player_id     int not null,
    match_id      int not null,
    runs_scored   INTEGER,
    balls_played  INTEGER,
    wickets_taken INTEGER,
    balls_bowled  INTEGER,
    position_in   INTEGER,
    FOREIGN KEY (player_id) REFERENCES player (id),
    FOREIGN KEY (match_id) REFERENCES match (match_id)
);

create table match_ball
(
    id           int not null AUTO_INCREMENT,
    match_id     int not null,
    ball_id      int not null,
    team_batting int not null,
    team_bowling int not null,
    bowler       int not null,
    striker      int not null,
    non_striker  int not null,
    runs_scored  int not null,
    is_wicket    bool,
    is_extra     bool,
    wicket_type  varchar(244),
    extra_type   varchar(244),
    foreign key (match_id) REFERENCES match (match_id),
    foreign key (team_batting) REFERENCES team (team_id),
    foreign key (team_bowling) references team (team_id),
    foreign key (bowler) references player (id),
    foreign key (striker) references player (id),
    foreign key (non_striker) references player (id),
)