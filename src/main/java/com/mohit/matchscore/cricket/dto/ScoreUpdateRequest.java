package com.mohit.matchscore.cricket.dto;

public class ScoreUpdateRequest {

    private int id;

    private int matchId;

    private int ballId;

    private int teamBatting;

    private int teamBowling;

    private int bowler;

    private int striker;

    private int nonStriker;

    private int runsScored;

    private boolean isWicket;

    private boolean isExtra;

    private String wicketType;

    private String extraType;

    @Override
    public String toString() {
        return "ScoreUpdateRequest{" +
                "id=" + id +
                ", matchId=" + matchId +
                ", ballId=" + ballId +
                ", teamBatting=" + teamBatting +
                ", teamBowling=" + teamBowling +
                ", bowler=" + bowler +
                ", striker=" + striker +
                ", nonStriker=" + nonStriker +
                ", runsScored=" + runsScored +
                ", isWicket=" + isWicket +
                ", isExtra=" + isExtra +
                ", wicketType='" + wicketType + '\'' +
                ", extraType='" + extraType + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getBallId() {
        return ballId;
    }

    public void setBallId(int ballId) {
        this.ballId = ballId;
    }

    public int getTeamBatting() {
        return teamBatting;
    }

    public void setTeamBatting(int teamBatting) {
        this.teamBatting = teamBatting;
    }

    public int getTeamBowling() {
        return teamBowling;
    }

    public void setTeamBowling(int teamBowling) {
        this.teamBowling = teamBowling;
    }

    public int getBowler() {
        return bowler;
    }

    public void setBowler(int bowler) {
        this.bowler = bowler;
    }

    public int getStriker() {
        return striker;
    }

    public void setStriker(int striker) {
        this.striker = striker;
    }

    public int getNonStriker() {
        return nonStriker;
    }

    public void setNonStriker(int nonStriker) {
        this.nonStriker = nonStriker;
    }

    public int getRunsScored() {
        return runsScored;
    }

    public void setRunsScored(int runsScored) {
        this.runsScored = runsScored;
    }

    public boolean isWicket() {
        return isWicket;
    }

    public void setWicket(boolean wicket) {
        isWicket = wicket;
    }

    public boolean isExtra() {
        return isExtra;
    }

    public void setExtra(boolean extra) {
        isExtra = extra;
    }

    public String getWicketType() {
        return wicketType;
    }

    public void setWicketType(String wicketType) {
        this.wicketType = wicketType;
    }

    public String getExtraType() {
        return extraType;
    }

    public void setExtraType(String extraType) {
        this.extraType = extraType;
    }
}
