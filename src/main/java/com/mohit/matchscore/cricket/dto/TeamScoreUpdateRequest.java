package com.mohit.matchscore.cricket.dto;

public class TeamScoreUpdateRequest {

    private int matchId;

    private int teamId;

    private int lastBallRuns;

    private boolean isExtra;

    private boolean isOut;

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getLastBallRuns() {
        return lastBallRuns;
    }

    public void setLastBallRuns(int lastBallRuns) {
        this.lastBallRuns = lastBallRuns;
    }

    public boolean isExtra() {
        return isExtra;
    }

    public void setExtra(boolean extra) {
        isExtra = extra;
    }

    public boolean isOut() {
        return isOut;
    }

    public void setOut(boolean out) {
        isOut = out;
    }

    @Override
    public String toString() {
        return "TeamScoreUpdateRequest{" +
                "matchId=" + matchId +
                ", teamId=" + teamId +
                ", lastBallRuns=" + lastBallRuns +
                ", isExtra=" + isExtra +
                ", isOut=" + isOut +
                '}';
    }
}
