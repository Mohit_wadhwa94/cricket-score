package com.mohit.matchscore.cricket.dto;

public class PlayerScoreUpdateRequest {

    private int playerId;

    private int matchId;

    private int runsScored;

    private int ballsPlayed;

    private int wicketsTaken;

    private int ballsBowled;

    private int isOut;

    private boolean isBatting;

    private boolean isBowling;

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getRunsScored() {
        return runsScored;
    }

    public void setRunsScored(int runsScored) {
        this.runsScored = runsScored;
    }

    public int getBallsPlayed() {
        return ballsPlayed;
    }

    public void setBallsPlayed(int ballsPlayed) {
        this.ballsPlayed = ballsPlayed;
    }

    public int getWicketsTaken() {
        return wicketsTaken;
    }

    public void setWicketsTaken(int wicketsTaken) {
        this.wicketsTaken = wicketsTaken;
    }

    public int getBallsBowled() {
        return ballsBowled;
    }

    public void setBallsBowled(int ballsBowled) {
        this.ballsBowled = ballsBowled;
    }

    public int getIsOut() {
        return isOut;
    }

    public void setIsOut(int isOut) {
        this.isOut = isOut;
    }

    public boolean isBatting() {
        return isBatting;
    }

    public void setBatting(boolean batting) {
        isBatting = batting;
    }

    public boolean isBowling() {
        return isBowling;
    }

    public void setBowling(boolean bowling) {
        isBowling = bowling;
    }

    @Override
    public String toString() {
        return "PlayerScoreUpdateRequest{" +
                "playerId=" + playerId +
                ", matchId=" + matchId +
                ", runsScored=" + runsScored +
                ", ballsPlayed=" + ballsPlayed +
                ", wicketsTaken=" + wicketsTaken +
                ", ballsBowled=" + ballsBowled +
                ", isOut=" + isOut +
                ", isBatting=" + isBatting +
                ", isBowling=" + isBowling +
                '}';
    }
}
