package com.mohit.matchscore.cricket.controller;

import com.mohit.matchscore.cricket.dto.ScoreUpdateRequest;
import com.mohit.matchscore.cricket.dto.ScoreUpdateResponse;
import com.mohit.matchscore.cricket.service.ScoreUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class ScoreUpdateController {

    @Autowired
    ScoreUpdateService scoreUpdateService;


    //Api to be called from admin panel to update score
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/updateScore")
    public ScoreUpdateResponse updateScore(@RequestBody ScoreUpdateRequest request)
    {
        return scoreUpdateService.updateScore(request);
    }
}
