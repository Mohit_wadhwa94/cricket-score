package com.mohit.matchscore.cricket.service;

import com.mohit.matchscore.cricket.ScoreUpdateConstants;
import com.mohit.matchscore.cricket.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service("scoreUpdateService")
public class ScoreUpdateService {

    Logger logger = Logger.getLogger(ScoreUpdateService.class.getName());

    @Autowired
    TeamScoreUpdateService teamScoreUpdateService;

    @Autowired
    PlayerScoreUpdateService  playerScoreUpdateService;

    @Autowired
    JdbcTemplate jdbcTemplate;


    //method to update team score will take the params specified in ScoreUpdateRequest
    public ScoreUpdateResponse updateScore(ScoreUpdateRequest request) {
        ScoreUpdateResponse response = new ScoreUpdateResponse();
        ScoreUpdateConstants.ScoreUpdateResponseStatus.FAILURE.name();

        if (request == null) {
            return response;
        }

        int matchId = request.getMatchId();
        int ballId = request.getBallId();
        int teamBatting = request.getTeamBatting();
        int teamBowling = request.getTeamBowling();
        int bowler = request.getBowler();
        int striker = request.getStriker();
        int nonStriker = request.getNonStriker();
        int runsScored = request.getRunsScored();
        boolean isWicket = request.isWicket();
        boolean isExtra = request.isWicket();
        String wicketType = request.getWicketType();
        String extraType = request.getExtraType();

        String matchBallCreateStatus = createMatchBallEntry(new String[] {});

        if (matchBallCreateStatus.equalsIgnoreCase(ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name())) {
            //Fire event to kafka to update team score and player score asynchronously
//            teamScoreUpdateService.updateTeamScore(new TeamScoreUpdateRequest());
//            playerScoreUpdateService.updatePlayerScore(new PlayerScoreUpdateRequest());
        }

        return response;
    }

    //this method is called for every ball update and creates a new entry in match_ball
    // table based upon the parameters in ScoreUpdateRequest
    private String  createMatchBallEntry(String[] strings) {

        String sql = "insert into ...";

        int rowsAffected = jdbcTemplate.update(sql);

        if (rowsAffected == 1) {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name();
        } else {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.FAILURE.name();
        }
    }
}
