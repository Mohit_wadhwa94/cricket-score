package com.mohit.matchscore.cricket.service;

import com.mohit.matchscore.cricket.ScoreUpdateConstants;
import com.mohit.matchscore.cricket.dto.TeamScoreUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TeamScoreUpdateService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //Add a kafka listener here to update team score on every sevent receive
    public void updateTeamScore(String message /*,Acknowledge acknowledge*/) {

        TeamScoreUpdateRequest request = new TeamScoreUpdateRequest();
//        request = mapper.get(message);
        int matchId = request.getMatchId();
        int teamId = request.getTeamId();
        int lastBallRuns = request.getLastBallRuns();
        boolean isExtra = request.isExtra();
        boolean isOut = request.isOut();

        //call method to update team score in db
        String status = updateTeamScoreInDb(new String[]{});

        if (status.equalsIgnoreCase(ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name())) {
//            acknowledge.ack();
        } else {
            //acknowlege event to kafka
            //            acknowledge.ack();
        }
    }

    private String updateTeamScoreInDb(String[] strings) {
        String sql = "update team_score ...";

        int rowsUpdated = jdbcTemplate.update(sql);

        if (rowsUpdated == 1) {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name();
        } else {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.FAILURE.name();
        }
    }
}
