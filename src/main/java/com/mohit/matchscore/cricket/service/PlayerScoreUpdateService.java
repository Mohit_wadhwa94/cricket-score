package com.mohit.matchscore.cricket.service;

import com.mohit.matchscore.cricket.ScoreUpdateConstants;
import com.mohit.matchscore.cricket.dto.PlayerScoreUpdateRequest;
import com.mohit.matchscore.cricket.dto.ScoreUpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class PlayerScoreUpdateService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //Add a kafka listener here to update playerscore on every event receive
    public void updatePlayerScore(String message /*,Acknowledge acknowledge*/) {
        ScoreUpdateResponse response = new ScoreUpdateResponse();
        response.setStatus(ScoreUpdateConstants.ScoreUpdateResponseStatus.FAILURE.name());

        PlayerScoreUpdateRequest request = new PlayerScoreUpdateRequest();

//        request = mapper.get(message);

        int playerId = request.getPlayerId();
        int matchId = request.getMatchId();
        int runsScored = request.getRunsScored();
        int ballsPlayed = request.getBallsPlayed();
        int wicketsTaken = request.getWicketsTaken();
        int ballsBowled = request.getBallsBowled();
        int isOut = request.getIsOut();
        boolean isBatting = request.isBatting();
        boolean isBowling = request.isBowling();

        //call method to update team score in db
        String status = updatePlayerScoreInDb(new String[]{});

        if (status.equalsIgnoreCase(ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name())) {
//            acknowledge.ack();
        } else {
            //acknowlege event to kafka
            //            acknowledge.ack();
        }
    }

    private String updatePlayerScoreInDb(String[] strings) {
        String sql = "update player_score ...";

        int rowsUpdated = jdbcTemplate.update(sql);

        if (rowsUpdated == 1) {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.SUCCESS.name();
        } else {
            return ScoreUpdateConstants.ScoreUpdateResponseStatus.FAILURE.name();
        }
    }
}
